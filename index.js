#!/usr/bin/env node
'use strict';

let text = process.argv[2];
let tweetWidth = process.argv[3];
let tweetstorm = require('./Tweetstorm');

tweetstorm.printTweets(text, tweetWidth);
