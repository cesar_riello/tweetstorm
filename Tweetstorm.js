'use strict';

const TWEET_SIZE = 140;

module.exports = {
  estimateAmountOfTweets: function (text, width, preTextWidth) {
    if (!text.length) {
      return 0;
    }
    return Math.ceil(text.length / (width - preTextWidth));
  },
  sanatizeText: function (text) {
    return (text || '').replace(/[\n\r\t]/g, ' ').replace(/[\s]{2}/g, ' ').trim();
  },
  calculatePreTextWidth: function (amountOfTweets) {
    return ((amountOfTweets || 0).toString().length * 2) + 2; // length of '1/1 '
  },
  calculateTweetWidth: function (text, tweetWidth) {
    if (!tweetWidth || !Number.isInteger(tweetWidth) || tweetWidth < 0) {
      tweetWidth = TWEET_SIZE;
    }
    let amountOfTweets = this.estimateAmountOfTweets(text, tweetWidth, this.calculatePreTextWidth());
    let preTextWidth = this.calculatePreTextWidth(amountOfTweets);
    return tweetWidth > preTextWidth ? tweetWidth - preTextWidth : tweetWidth;
  },
  getTweets: function (text, tweetWidth) {
    tweetWidth = Number(tweetWidth);
    text = this.sanatizeText(text);
    let calculatedTweetWidth = this.calculateTweetWidth(text, tweetWidth);
    return this.populateWithIndex(this.splitTweets(text, calculatedTweetWidth));
  },
  splitTweets: function (text, tweetWidth) {
    let tweets = [];

    while (text.length >= 1) {
      if (text.length < tweetWidth) {
        tweets.push(text);
        break;
      }

      let sliceIndex = text.lastIndexOf(' ', tweetWidth);
      if (sliceIndex === -1) {
        throw Error('Your text have a word bigger then 140 characters.');
      }

      tweets.push(text.substring(0, sliceIndex));
      text = text.slice(sliceIndex + 1, text.length);
    }
    return tweets;
  },
  populateWithIndex: function (tweets) {
    let total = tweets.length;
    return tweets.map(function (tweet, index){
      return `${(index + 1)}/${total} ${tweet}`;
    });
  },
  printTweets: function (text, tweetWidth) {
    let tweets = this.getTweets(text, tweetWidth);
    console.log('\n\nTHE STORM IS COMING\n');
    tweets.map(function (tweet) {
      console.log(`${tweet} (${tweet.length})\n`);
    });
  }
};
