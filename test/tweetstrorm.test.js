'use strict';

const assert = require('assert');
let tweetstorm  = require('../Tweetstorm');

describe('Tweetstorm', function() {

  let mockSmallText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';

  let mockMediumText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac ligula sed nibh sagittis accumsan. Curabitur gravida erat vel augue tinc' +
  'vitae porta justo lobortis. Sed sed pretium massa, vel volutpat lectus. Maecenas ornare volutpat turpis, commodo vulputate ligula mattis' +
  'dapibus. Praesent a sodales neque, at consectetur elit. Donec luctus sapien massa, ac varius tellus tincidunt nec.';

  let mockBigText = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac ligula sed nibh sagittis accumsan. Curabitur gravida erat vel augue
  tincidunt, vitae porta justo lobortis. Sed sed pretium massa, vel volutpat lectus. Maecenas ornare volutpat turpis, commodo vulputate
  ligula mattis dapibus. Praesent a sodales neque, at consectetur elit. Donec luctus sapien massa, ac varius tellus tincidunt nec.
  Praesent tempor nulla non eros elementum venenatis vel ut ante. Duis eleifend in justo eget mattis. Phasellus commodo lacus nisl,
  in aliquet sapien rutrum eget. Donec et hendrerit sapien, in laoreet eros. Cras leo justo, aliquet id ligula at, auctor cursus orci.
  Proin pharetra sodales magna nec maximus. Aenean urna ipsum, scelerisque at blandit vitae, auctor eget turpis. Donec eget sagittis
  purus. Ut sed vulputate lectus. `; //with line breaks

  let mockHugeText = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac ligula sed nibh sagittis accumsan. Curabitur gravida erat vel augue
  tincidunt, vitae porta justo lobortis. Sed sed pretium massa, vel volutpat lectus. Maecenas ornare volutpat turpis, commodo vulputate
  ligula mattis dapibus. Praesent a sodales neque, at consectetur elit. Donec luctus sapien massa, ac varius tellus tincidunt nec.
  Praesent tempor nulla non eros elementum venenatis vel ut ante. Duis eleifend in justo eget mattis. Phasellus commodo lacus nisl,
  in aliquet sapien rutrum eget. Donec et hendrerit sapien, in laoreet eros. Cras leo justo, aliquet id ligula at, auctor cursus orci.
  Proin pharetra sodales magna nec maximus. Aenean urna ipsum, scelerisque at blandit vitae, auctor eget turpis. Donec eget sagittis
  purus. Ut sed vulputate lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac ligula sed nibh sagittis accumsan.
  Curabitur gravida erat vel augue tincidunt, vitae porta justo lobortis. Sed sed pretium massa, vel volutpat lectus. Maecenas ornare
  volutpat turpis, commodo vulputate ligula mattis dapibus. Praesent a sodales neque, at consectetur elit. Donec luctus sapien massa,
  ac varius tellus tincidunt nec. Praesent tempor nulla non eros elementum venenatis vel ut ante. Duis eleifend in justo eget mattis.
  Phasellus commodo lacus nisl, in aliquet sapien rutrum eget. Donec et hendrerit sapien, in laoreet eros. Cras leo justo, aliquet id
  ligula at, auctor cursus orci. Proin pharetra sodales magna nec maximus. Aenean urna ipsum, scelerisque at blandit vitae, auctor eget
  turpis. Donec eget sagittis purus. Ut sed vulputate lectus. `; //with line breaks

  it('#estimateAmountOfTweets', function() {
    assert.equal(
      tweetstorm.estimateAmountOfTweets('', 140, 3),
      0
    );

    assert.equal(
      tweetstorm.estimateAmountOfTweets(mockSmallText, 140, 3),
      1
    );

    assert.equal(
      tweetstorm.estimateAmountOfTweets(mockMediumText, 140, 3),
      3
    );

    assert.equal(
      tweetstorm.estimateAmountOfTweets(mockBigText, 140, 3),
      7
    );

    assert.equal(
      tweetstorm.estimateAmountOfTweets(mockHugeText, 140, 5),
      13
    );

  });

  it('#calculatePreTextWidth', function() {
    assert.equal(tweetstorm.calculatePreTextWidth(0), 4);

    assert.equal(tweetstorm.calculatePreTextWidth(1), 4);

    assert.equal(tweetstorm.calculatePreTextWidth(9), 4);

    assert.equal(tweetstorm.calculatePreTextWidth(10), 6);

    assert.equal(tweetstorm.calculatePreTextWidth(99), 6);

    assert.equal(tweetstorm.calculatePreTextWidth(100), 8);

    assert.equal(tweetstorm.calculatePreTextWidth(1357), 10);

  });

  it('#splitTweets', function() {
    assert.deepEqual(
      tweetstorm.splitTweets(mockSmallText, 140),
      [mockSmallText]
    );

    assert.deepEqual(
      tweetstorm.splitTweets(mockMediumText, 140),
      [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac ligula sed nibh sagittis accumsan. Curabitur gravida erat vel augue",
        "tincvitae porta justo lobortis. Sed sed pretium massa, vel volutpat lectus. Maecenas ornare volutpat turpis, commodo vulputate ligula",
        "mattisdapibus. Praesent a sodales neque, at consectetur elit. Donec luctus sapien massa, ac varius tellus tincidunt nec."
      ]
    );

    assert.deepEqual(tweetstorm.splitTweets('', 5), []);

    assert.deepEqual(
      tweetstorm.splitTweets('qwe asd zxc rty', 5),
      ['qwe', 'asd', 'zxc', 'rty']
    );

  });

  it('#splitTweets - big word exception', function() {
    assert.throws(
      () => tweetstorm.splitTweets('qwe asd zxc rty', 2),
      Error,
      'Your text have a word bigger then 140 characters.'
    );

  });

  it('#populateWithIndex', function() {
    assert.deepEqual(
      tweetstorm.populateWithIndex(['qwe', 'asd', 'zxc', 'rty']),
      ['1/4 qwe', '2/4 asd', '3/4 zxc', '4/4 rty']
    );

    assert.deepEqual(
      tweetstorm.populateWithIndex(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']),
      ['1/12 1', '2/12 2', '3/12 3', '4/12 4', '5/12 5', '6/12 6', '7/12 7', '8/12 8', '9/12 9', '10/12 10', '11/12 11', '12/12 12']
    );

  });

  it('#calculateTweetWidth', function() {
    assert.deepEqual(
      tweetstorm.calculateTweetWidth(mockSmallText, 140),
      136
    );

    assert.deepEqual(
      tweetstorm.calculateTweetWidth('qwe asd zxc rty', 5),
      5
    );

    assert.deepEqual(
      tweetstorm.calculateTweetWidth('qwe asd zxc rty', 6),
      2
    );

  });

  it('#sanatizeText', function() {

    let textWithLineBreaks = ` one line
other line
and more one `;
    assert.equal(
      tweetstorm.sanatizeText(textWithLineBreaks),
      'one line other line and more one'
    );
  });


});
